FROM node:16.19.1 as builder

ENV NODE_ENV development

RUN echo " ------------------Web打包 --------------------"

WORKDIR /management-web

COPY . /management-web

RUN npm install 
RUN npm run build

RUN echo " ------------------Web容器部署启动 --------------------"

FROM nginx:1.19.2
COPY --from=builder /management-web/build /usr/share/nginx/html
COPY deploy/nginx/conf.d /etc/nginx/conf.d
EXPOSE 80
